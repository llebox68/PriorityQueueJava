

public class ArrayExtended<E> 
{
	public static final int SIZE = 2;
	private E [] arr;
	private int size = 0;
	
	public ArrayExtended()
	{
		this(SIZE);
	}
	
	public ArrayExtended(int SIZE)
	{
		arr = (E[]) new Object[SIZE];
	}
	
	public int size()
	{
		return size;
	}
	
	public boolean isEmpty()
	{
		return (size == 0);
	}
	
	public E get(int i) throws IndexOutOfBoundsException
	{
		checkIndex(i, size);
		return arr[i];
	}
	
	public E set(int i, E e) throws IndexOutOfBoundsException
	{
		checkIndex(i, size);
		E temp = arr[i];
		arr[i] = e;
		return temp;
	}
	
	public void add(E e) throws IndexOutOfBoundsException, IllegalStateException
	{
		//checkIndex(i, size + 1);
		if (size == arr.length)
		{
			//System.out.println("extending array size!");
			resize(2 * arr.length);
		}
		//for (int k = size - 1; k >= i; k++)
		//	arr[k + 1] = arr[k];
		arr[size] = e;
		size++;
	}
	
	public void addToFirst(E e)  throws IndexOutOfBoundsException, IllegalStateException
	{
		//checkIndex(i, size + 1);
		if (size == arr.length)
		{
			//System.out.println("extending array size!");
			resize(2 * arr.length);
		}
		for (int k = size - 1; k > 0; k++)
			arr[k + 1] = arr[k];
		arr[0] = e;
		size++;
	}
	
	public E remove(int i) throws IndexOutOfBoundsException
	{
		checkIndex(i, size);
		E temp = arr[i];
		for (int k = i; k < size - 1; k++)
			arr[k] = arr[k + 1];
		arr[size - 1] = null;
		size--;
		return temp;
	}
	
	private void checkIndex(int i, int n) throws IndexOutOfBoundsException
	{
		if (i < 0 || i >= n)
			throw new IndexOutOfBoundsException("Illegal index found at:  " + i);
	}
	
	private void resize(int SIZE)
	{
		E temp [] = (E[]) new Object[SIZE];
		for (int k = 0; k < size; k++)
			temp[k] = arr[k];
		arr = temp;
	}
	
}