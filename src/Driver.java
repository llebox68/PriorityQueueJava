import java.util.Comparator;

public class Driver 
{ 
	
	public static void Algo(int n)
	{
		if (n > 1)
		{
			Algo(n/2);
			Algo(n/2);
			System.out.println("with n = " + n + "*");
		}
	}
	
	public static void qsInPlace(int[] s, int a, int b)
	{
		if (a >= b) return;
		int left = a;
		int right = b - 1;
		int pivot = s[b];
		int temp;
		
		System.out.println("left index: " + a + "                pivot index: " + b);
		while (left <= right)
		{
			while (left <= right && s[left] < pivot)
				left ++;
			while (left <= right && s[right] > pivot)
				right --;
			
			System.out.println("left index in loop: " + left + "        right index in loop: " + right);
			
			if (left <= right)
			{
				for(int i = 0; i < s.length; i++)
					System.out.print(s[i] + " ");
				System.out.println();
				
				temp = s[left];
				s[left] = s[right];
				s[right] = temp;
				left++;
				right--;
			}
			
			for(int i = 0; i < s.length; i++)
				System.out.print(s[i] + " ");
			System.out.println();
		}
		
		temp = s[left];
		s[left] = s[b];
		s[b] = temp;
		
		for(int i = 0; i < s.length; i++)
			System.out.print(s[i] + " ");
		System.out.println();
		
		qsInPlace(s, a, left - 1);
		qsInPlace(s, left + 1, b);
	}
	
	public static void main(String [] args)
	{
		
		Algo(5);
		
		int [] s = {38, 81, 22, 48, 13, 69, 93, 14, 45, 58, 13, 79, 72};
		qsInPlace(s, 0, s.length - 1);
		
		for (int i = 0; i < s.length; i++)
			System.out.print(s[i] + " ");
		
		//create empty priority queue
		PriorityQueue<Integer,Integer> pq = new PriorityQueue<>();	
			
		//check if empty pq
		System.out.println("\nempty pq? " + pq.isEmpty());
		
		//switch from min state to max state
		System.out.println("Switching to Max state (if already is, then just print the current state!");
		pq.switchToMax();
		
		//double check with enum variable
		System.out.println("state: " + pq.state());
		
		//add elements into the priority queue
		pq.insert(99, 8);
		System.out.println("\n");
		pq.PQPrint();
		pq.insert(100, 11);
		System.out.println("\n");
		pq.PQPrint();
		pq.insert(1, 23);
		System.out.println("\n");
		pq.PQPrint();
		pq.insert(3, 11);
		System.out.println("\n");
		pq.PQPrint();
		pq.insert(2, 12);
		System.out.println("\n");
		pq.PQPrint();
		pq.insert(4, 9);
		System.out.println("\n");
		pq.PQPrint();
		pq.insert(22, 11);
		System.out.println("\n");
		pq.PQPrint();
		pq.insert(33, 15);
		System.out.println("\n");
		
		//print the current priority queue
		pq.PQPrint();
		pq.pqSort(pq);
		pq.PQPrint();
		
		//print top()
		System.out.println("\n\nTop: " + "(" + pq.top().getKey() + "," + pq.top().getVal() + ") ");
		System.out.println("Remove top: ");
		pq.remove();
		pq.PQPrint();

		//switch current state to min from max and add elements
		System.out.println("Switching from Max to Min State!");
		pq.switchToMin();
		
		//add elements into the priority queue
		pq.insert(9, 11);
		pq.insert(100, 14);
		
		System.out.println("\nRemove top: ");
		pq.remove();
		pq.PQPrint();
		
		//use downheap
		System.out.println("\nDownheap on 1st index: ");
		pq.downheap(0);
		pq.PQPrint();
		pq.insert(1, 2);
		
		//use upheap
		System.out.println("\nUpheap on 4th index: ");
		pq.upheap(pq.size() - 1);
		pq.PQPrint();
		
		//use swap
		System.out.println("\nSwap between 1st and 2nd index: ");
		pq.swap(0, 1);
		pq.PQPrint();
		
		//use size
		System.out.println("\ncurrent size: " + pq.size());
		
		//sort using pqSort
		pq.pqSort(pq);
		pq.PQPrint();

	}
}
