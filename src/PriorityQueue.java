import java.util.Comparator;

public class PriorityQueue<K,V>
{
	private enum State {MAX, MIN};
	private State st = State.MIN;
	private ArrayExtended<PQEntry<K,V>> heap  = new ArrayExtended<>();
	private int size = 0;
	private Comparator<K> c;
	
	
	//return current state
	public State state()
	{
		return st;
	}
	
	//only using protected so it can be accessed in the driver's class
	protected class PQEntry<K,V>
	{
		private K k;
		private V v;

		
		public PQEntry(K k, V v)
		{
			this.k = k;
			this.v = v;
		}
		
		public K getKey()
		{
			return k;
		}
		
		public V getVal()
		{
			return v;
		}
		
		public void setKey(K k)
		{
			this.k = k;
		}
		
		public void setVal(V v)
		{
			this.v = v;
		}
	}
	
	
	public PriorityQueue(Comparator<K> c)
	{
		this.c = c;
	}
	
	public PriorityQueue()
	{
		this(new DefaultComparator<K>());
	}
	
	//compare the key of a given priority queue with (K,V)
	public int compare(PQEntry<K,V> x, PQEntry<K,V> y)
	{
		return c.compare(x.getKey(), y.getKey());
	}
	
	private boolean checkKey(K key) throws IllegalArgumentException
	{
		try
		{
			return (c.compare(key,key) == 0);
		}
		
		catch (ClassCastException e)
		{
			throw new IllegalArgumentException("Incompatible key!");
		}
	}
	
	public boolean isEmpty()
	{
		return (size == 0);
	}
	
	/*
	 * check for parent, left, right nodes
	 */
	
	public int parent(int i)
	{
		return ((i - 1) / 2);
	}
	
	public int left(int i)
	{
		return (2 * i + 1);
	}
	
	public int right(int i)
	{
		return (2 * i + 2);
	}
	
	public boolean hasLeft(int i)
	{
		return (left(i) < heap.size());
	}
	
	public boolean hasRight(int i)
	{
		return (right(i) < heap.size());
	}
	
	//swap between node i and j
	public void swap(int i, int j)
	{
		PQEntry<K,V> temp = heap.get(i);
		heap.set(i, heap.get(j));
		heap.set(j, temp);
	}
	
	public void upheap(int i)
	{
		while (i > 0)
		{
			//get parent index in arraylist
			int p = parent(i);
			
			if (st == State.MIN)
			{
				//break the loop if parent is greater than its child
				if (compare(heap.get(i), heap.get(p)) >= 0) 
					break;
			}
			else if (st == State.MAX)
			{
				if (compare(heap.get(i), heap.get(p)) <= 0)
					break;
			}
			swap(i, p);
			i = p;
		}
	}
	
	/* 
	 * transforms a max- to a min-priority queue; else leave unchanged.
	 */
	public void switchToMin()
	{
		if (st == State.MAX)
			toggle();
		System.out.println("\ncurrent state: " + st);
	}
	
	/* 
	 * transforms a min- to a max-priority queue; else leave unchanged.
	 */
	public void switchToMax()
	{
		if (st == State.MIN)
			toggle();
		System.out.println("\ncurrent state: " + st);
	}
	
	public void downheap(int i)
	{
		while (hasLeft(i))
		{
			int leftIndex = left(i);
			int smallChildIndex = leftIndex;
			
			if (hasRight(i))
			{
				int rightIndex = right(i);
				
				//check which of the child is bigger, track the smallest index
				if (st == State.MAX)
				{
					if (compare(heap.get(leftIndex),heap.get(rightIndex)) < 0)
						smallChildIndex = rightIndex;		
				}
				
				else if (st == State.MIN)
				{
					if (compare(heap.get(leftIndex),heap.get(rightIndex)) > 0)
						smallChildIndex = rightIndex;
				}
			}
			
			//System.out.println("smallChilIndex: " + heap.get(smallChildIndex).getKey());
			
			if (st == State.MIN)
			{
				if (compare(heap.get(smallChildIndex), heap.get(i)) >= 0)
					break;
			}
			
			else if (st == State.MAX)
			{
				if (compare(heap.get(smallChildIndex), heap.get(i)) <= 0)
					break;
			}
			
			//else if the latter case is not executed, we have a greater parent index than its child's index
			swap(i, smallChildIndex);
			i = smallChildIndex;
		}
	}
	
	//get the size of the priority queue
	public int size()
	{
		return size;
	}
	
	
	/*
	 * Insert the (k,v) entry which is a key(k), value(v) pair to the priority queue.
	 * create a new pq, add it into the heap (at the bottom), then re-organize the heap using upheap method and return the pq.
	 */
	public PQEntry<K,V> insert(K key, V value) throws IllegalArgumentException
	{
		checkKey(key);
		
		//create new position with given (K,V)
		PQEntry<K,V> pq = new PQEntry<>(key, value);
		
		//add pq to the end of the heap
		heap.add(pq);
		
		//compare this element with its parent, swap position depending on current state
		upheap(heap.size() - 1);
		size++;
		return pq;
	}
		
	/*
	 * returns the top entry (with the minimum or the maximum key depending on whether it is a Min- or Max-priority queue, without removing the entry.
	 */
	public PQEntry<K,V> top()
	{
		if (size == 0)
			return null;
		return heap.get(0);
	}
	
	/*
	 * transforms a min- to a max-priority queue or vice versa.
	 */
	public void toggle()
	{
		if (st == State.MAX)
			st = State.MIN;
		else if (st == State.MIN)
			st = State.MAX;
	}
	
	
	/*	
	 * removes and returns the entry (a key, value pair) with the smallest or biggest key
	 * depending on the current state of the priority queue (either Min or Max).
	*/
	public PQEntry<K,V> remove()
	{
		if (heap.isEmpty())
			return null;
		
		//get the root of tree, max/min element depending on the state of the PQ
		PQEntry<K,V> init = heap.get(0);
		
		//swap location with the last item in tree
		swap(0, heap.size() - 1);
		
		//remove the old root from tree
		heap.remove(heap.size() - 1);
		
		//reorder the tree with the new root and its descendants
		downheap(0);
		size--;
		return init;
	}
		
	public void PQPrint()
	{
		for(int i = 0; i < heap.size(); i++)
			System.out.print("(" + heap.get(i).getKey() + "," + heap.get(i).getVal() + ") ");	
	}
	
	public void pqSort(PriorityQueue<K,V> L)
	{
		//create a new pq to hold new (K,V)
		PriorityQueue<K,V> pq = new PriorityQueue<>();
		
		//elements to be removed and added
		PriorityQueue<K,V>.PQEntry<K, V> e1;
		PriorityQueue<K,V>.PQEntry<K, V> newel;
		
		//with the given list, remove element one by one and put them into the new list
		while (!L.isEmpty())
		{
			e1 = L.remove();
			pq.insert(e1.getKey(), e1.getVal());
			System.out.println("remove key: " + e1.getKey());
		}
		
		//given the old list is empty, put every element back into the original list while being sorted
		while (!pq.isEmpty())
		{
			newel = pq.remove();
			L.addToLast(newel.getKey(), newel.getVal());
			System.out.println("add key: (" + newel.getKey() + "," + newel.getVal() + ")");
			//L.PQPrint();
		}
	}
	
	public PQEntry<K,V> addToLast(K key, V val)
	{
		checkKey(key);
		PQEntry<K,V> pq = new PQEntry<>(key,val);
		heap.addToFirst(pq);
		return pq;
	}
}
